# Simple multi step form

The Simple multi step form module provides the ability to convert any entity form into a simple multi-step form. There is no restriction on the number of steps (in terms of performance).

- For a full description of the module, visit the
  [Project Page](https://www.drupal.org/project/simple_multistep)

- Submit bug reports and feature suggestions, or track changes in the
  [Issue Queue](https://www.drupal.org/project/issues/simple/_multistep)


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module will requires field\_group module dependencies.

## Installation

- Install as you would normally install a contributed Drupal module.
  Visit: [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) for further information.

## Configuration

- Install simple_multistep module (this module require fieldgroup
  module).
- Go to Manage form display and click “Add group”.
- Choose Form step in drop down and configure this field group as you
  like.

## Maintainers

Current maintainers:

- Vitaliy Bogomazyuk - [VitaliyB98](https://www.drupal.org/u/vitaliyb98)
- Andrei Vesterli - [andrei.vesterli](https://www.drupal.org/u/andreivesterli)
- Oleksiy Kalinichenko - [AexChecker](https://www.drupal.org/u/aexchecker)
- Yuriy Bortnik - [drakemazzy](https://www.drupal.org/u/drakemazzy)

Supporting organizations:

- [AnyforSoft](https://www.drupal.org/anyforsoft)

This project has been sponsored by:

- [Internetdevels](https://www.drupal.org/internetdevels)
